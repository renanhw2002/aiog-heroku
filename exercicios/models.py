from django.db import models
from django.conf import settings
from django.utils import timezone


class Exercicio(models.Model):
    data_publicacao = models.DateTimeField(default=timezone.now)
    imagem_card_url = models.URLField(max_length=999, null=True)
    nome_exercicio = models.CharField(max_length=999, default="")
    descricao_exercicio = models.TextField(default="")
    texto_html = models.TextField(default="")
    imagem_post_url = models.URLField(max_length=999, null=True)


class Comment(models.Model):
    data_publicacao = models.DateTimeField(default=timezone.now)
    autor = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    texto = models.CharField(max_length=255)
    likes = models.IntegerField(default=0)
    post = models.ForeignKey(Exercicio, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.texto}" - {self.autor.username}'


class Category(models.Model):
    nome_categoria = models.CharField(max_length=255, default="")
    descricao_categoria = models.CharField(max_length=255, default="")
    exercicios = models.ManyToManyField(Exercicio)

    def __str__(self):
        return f'{self.nome_categoria}'