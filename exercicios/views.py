from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from .models import Exercicio, Comment, Category
from .forms import ExercicioForm, CommentForm, CategoryForm



class ExercicioListView(generic.ListView):
    model = Exercicio
    template_name = 'exercicios/index.html'




def detail_exercicio(request, exercicio_id):
    exercicio = get_object_or_404(Exercicio, pk=exercicio_id)
    context = {'exercicio': exercicio}
    return render(request, 'exercicios/detail.html', context)





def search_exercicio(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        lista_exercicios = Exercicio.objects.filter(name__icontains=search_term)
        context = {"lista_exercicios": lista_exercicios}
    return render(request, 'exercicios/search.html', context)





def create_exercicio(request):
    if request.method == 'POST':
        form = ExercicioForm(request.POST)
        if form.is_valid():
            imagem_card_url = form.cleaned_data['imagem_card_url']
            nome_exercicio = form.cleaned_data['nome_exercicio']
            descricao_exercicio = form.cleaned_data['descricao_exercicio']
            texto_html = form.cleaned_data['texto_html']
            imagem_post_url = form.cleaned_data['imagem_post_url']
            exercicio = Exercicio(imagem_card_url=imagem_card_url,
                        nome_exercicio=nome_exercicio,
                        descricao_exercicio=descricao_exercicio,
                        texto_html=texto_html,
                        imagem_post_url=imagem_post_url)
            exercicio.save()
            return HttpResponseRedirect(
                reverse('exercicios:detail', args=(exercicio.id, )))
    else:
        form = ExercicioForm()
    context = {'form': form}
    return render(request, 'exercicios/create.html', context)




def update_exercicio(request, exercicio_id):
    exercicio = get_object_or_404(Exercicio, pk=exercicio_id)

    if request.method == "POST":
        form = ExercicioForm(request.POST)
        if form.is_valid():
            imagem_card_url = form.cleaned_data['imagem_card_url']
            nome_exercicio = form.cleaned_data['nome_exercicio']
            descricao_exercicio = form.cleaned_data['descricao_exercicio']
            texto_html = form.cleaned_data['texto_html']
            imagem_post_url = form.cleaned_data['imagem_post_url']
            exercicio = Exercicio(imagem_card_url=imagem_card_url,
                        nome_exercicio=nome_exercicio,
                        descricao_exercicio=descricao_exercicio,
                        texto_html=texto_html,
                        imagem_post_url=imagem_post_url)
            exercicio.save()
            return HttpResponseRedirect(
                reverse('exercicios:detail', args=(exercicio.id, )))
    else:
        form = ExercicioForm(
            initial={
                'imagem_card_url': exercicio.imagem_card_url,
                'nome_exercicio': exercicio.nome_exercicio,
                'descricao_exercicio': exercicio.descricao_exercicio,
                'texto_html': exercicio.texto_html,
                'imagem_post_url': exercicio.imagem_post_url,
            })
    context = {'exercicio': exercicio, 'form': form}
    return render(request, 'exercicios/update.html', context)





def delete_exercicio(request, exercicio_id):
    exercicio = get_object_or_404(Exercicio, pk=exercicio_id)

    if request.method == "POST":
        exercicio.delete()
        return HttpResponseRedirect(reverse('exercicios:index'))

    context = {'exercicio': exercicio}
    return render(request, 'exercicios/delete.html', context)



def create_comment(request, exercicio_id):
    exercicio = get_object_or_404(Exercicio, pk=exercicio_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_autor = form.cleaned_data['autor']
            comment_texto = form.cleaned_data['texto']
            comment = Comment(autor=comment_autor,
                            texto=comment_texto,
                            post = exercicio)
            comment.save()
            return HttpResponseRedirect(
                reverse('exercicios:detail', args=(exercicio_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'exercicio': exercicio}
    return render(request, 'exercicios/comment.html', context)




class CategoryListView(generic.ListView):
    model = Category
    template_name = 'exercicios/categorias.html'


class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'exercicios/create_categoria.html'
    fields = ['nome_categoria', 'descricao_categoria', 'exercicios']
    success_url = reverse_lazy('exercicios:categorias')