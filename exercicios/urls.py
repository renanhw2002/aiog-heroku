from django.urls import path

from . import views

app_name = 'exercicios'
urlpatterns = [
    path('', views.ExercicioListView.as_view(), name='index'),
#    path('search/', views.search_exercicios, name='search'),
    path('create/', views.create_exercicio, name='create'),
    path('<int:exercicio_id>/', views.detail_exercicio,
         name='detail'), 
    path('update/<int:exercicio_id>/', views.update_exercicio, name='update'),
    path('delete/<int:exercicio_id>/', views.delete_exercicio, name='delete'),
    path('<int:exercicio_id>/comment/', views.create_comment, name='comment'),
    path('categorias/', views.CategoryListView.as_view(), name='categorias'),
    path('categorias/create_categoria', views.CategoryCreateView.as_view(), name='create_categoria'),
#    path('import/', views.import_exercicio, name='import'),
]
