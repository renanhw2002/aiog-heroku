from django.forms import ModelForm
from .models import Exercicio, Comment, Category


class ExercicioForm(ModelForm):
    class Meta:
        model = Exercicio
        fields = [
            'imagem_card_url',
            'nome_exercicio',
            'descricao_exercicio',
            'texto_html',
            'imagem_post_url',
        ]
        labels = {
            'imagem_card_url': 'URL da imagem para o card',
            'nome_exercicio': 'Nome do exercício',
            'descricao_exercicio': 'Descrição do exercício',
            'texto_html': 'Código em html do conteúdo do post',
            'imagem_post_url': 'URL da imagem para o post',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'autor',
            'texto',
        ]

        labels = {
            'autor': 'Autor da publicação',
            'texto': 'Seu comentário',
        }

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = [
            'nome_categoria', 
            'descricao_categoria',
            'exercicios',
        ]

        labels = {
            'nome_categoria': 'Nome da categoria/tag',
            'descricao_categoria': 'Descrição da categoria/tag',
            'exercicios': 'Exercícios que serão incluídos na categoria/tag',
        }