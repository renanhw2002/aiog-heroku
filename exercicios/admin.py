from django.contrib import admin

from .models import Exercicio, Comment, Category

admin.site.register(Exercicio)
admin.site.register(Comment)
admin.site.register(Category)