from django.urls import path

from . import views

urlpatterns = [
    path('nutricao/', views.nutricao, name='nutricao'),
    path('', views.index, name='index'),
    path('treino/', views.treino, name='treino'),
    path('treino/composicao_corporal', views.composicao_corporal, name='composicao_corporal'),
    path('treino/divisao_de_treino', views.divisao_de_treino, name='divisao_de_treino'),
    path('treino/periodizacao', views.periodizacao, name='periodizacao'),
]