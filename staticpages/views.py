from django.shortcuts import render


def index(request):
    context = {}
    return render(request, 'staticpages/index.html', context)

def nutricao(request):
    context = {}
    return render(request, 'staticpages/nutricao.html', context)

def treino(request):
    context = {}
    return render(request, 'staticpages/treino.html', context)

def composicao_corporal(request):
    context = {}
    return render(request, 'staticpages/treino/composicao_corporal.html', context)

def periodizacao(request):
    context = {}
    return render(request, 'staticpages/treino/periodizacao.html', context)

def divisao_de_treino(request):
    context = {}
    return render(request, 'staticpages/treino/divisao_de_treino.html', context)

